FROM alpine:latest
MAINTAINER tareqmohammadyousuf "tareq.y@gmail.com"

ENV FTP_USER=admin \
    FTP_PASS=admin \
    PASV_ADDRESS=tareq.vantageip.com \
    PASV_MIN=21100 \
    PASV_MAX=21110

RUN apk update && apk upgrade &&  apk --update --no-cache add vsftpd

RUN sed -i "s/anonymous_enable=YES/anonymous_enable=NO/" /etc/vsftpd/vsftpd.conf \
    && sed -i "s/.*local_enable=.*/local_enable=YES/" /etc/vsftpd/vsftpd.conf \
    && sed -i "s/.*ftpd_banner=.*/ftpd_banner=Welcome to tareq's vsftpd docker./" /etc/vsftpd/vsftpd.conf \
    && sed -i "s/.*listen_ipv6=.*/listen_ipv6=NO/" /etc/vsftpd/vsftpd.conf \
    && sed -i "s/.*dirmessage_enable=.*/dirmessage_enable=YES/" /etc/vsftpd/vsftpd.conf \
    && sed -i "s/.*write_enable=.*/write_enable=YES/" /etc/vsftpd/vsftpd.conf \
    && sed -i "s/.*local_umask=.*/local_umask=022/" /etc/vsftpd/vsftpd.conf

RUN echo "pasv_enable=Yes" >> /etc/vsftpd/vsftpd.conf \
  && echo "background=NO" >> /etc/vsftpd/vsftpd.conf \
  && echo "background=NO" >> /etc/vsftpd/vsftpd.conf \
  && echo "max_clients=10" >> /etc/vsftpd/vsftpd.conf \
  && echo "max_per_ip=5" >> /etc/vsftpd/vsftpd.conf \
  && echo "seccomp_sandbox=NO" >> /etc/vsftpd/vsftpd.conf

COPY vsftpd.sh /usr/sbin/
RUN chmod +x /usr/sbin/vsftpd.sh

EXPOSE 21 21100-21110

CMD /usr/sbin/vsftpd.sh

